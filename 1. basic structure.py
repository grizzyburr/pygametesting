import pygame

# <init> initializes all pygame modules
pygame.init()
# </init>

# <window>
width = 800
height = 600
screen = pygame.display.set_mode((width,height))
# </window>

# <player>
player = pygame.Rect((300,250,50,50))
# </player>

# <loop>
run = True
while run:
    screen.fill((0,0,0))
    pygame.draw.rect(screen,(255,0,0),player)
    # <key_events>
    key = pygame.key.get_pressed()
    if key[pygame.K_a] == True:
        player.move_ip(-1,0)
    elif key[pygame.K_w] == True:
        player.move_ip(0,-1)
    elif key[pygame.K_s] == True:
        player.move_ip(0,1)
    elif key[pygame.K_d] == True:
        player.move_ip(1,0)
    # </key_events>
    # <event_handler>
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
    # </event_handler>
    pygame.display.update()
# </loop>

# <quit> uninitializes all of the pygame modules initialied by .init() at start
pygame.quit()
# </quit>